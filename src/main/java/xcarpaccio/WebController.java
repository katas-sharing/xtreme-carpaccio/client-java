package xcarpaccio;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class WebController {

    private final Logger LOG = LoggerFactory.getLogger(WebController.class);

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public Amount answerQuote(@RequestBody final Order order) {
        LOG.info("Order received: " + order.toString());
        if (order.prices.length == 0)
            return new Amount(computeAmount(order));

        // Throw a 404 if you don't want to respond to an order, without penalty
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cannot answer");
    }

    Double computeAmount(final Order order) {
        return 0.0;
    }

    /** ----------------------------------------------------------- */

    @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public void logFeedback(@RequestBody final FeedbackMessage message) {
        LOG.info("Feedback received: " + message.toString());
    }

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String ping() {
        LOG.info("Ping received");
        return "Pong";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(final HttpServletRequest request) {
        return String.format("<html><body><h2>Status Page</h2><div>Your URL : <b>%s</b><body></html>",
                request.getRequestURL().toString());
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String error(final HttpServletRequest request) {
        final Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        final Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        return String.format(
                "<html><body><h2>Error Page</h2><div>Status code: <b>%s</b></div>"
                        + "<div>Exception Message: <b>%s</b></div><body></html>",
                statusCode, exception == null ? "N/A" : exception.getMessage());
    }
}